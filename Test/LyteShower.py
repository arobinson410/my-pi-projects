#Created By Andrew Robinson and Chris Burgess

import RPi.GPIO as GPIO
from time import sleep

temp = 80
Enc_A = 3 
Enc_B = 5 


def init():

    print "Lyte Shower Temperature Control Software"

    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(Enc_A, GPIO.IN) 
    GPIO.setup(Enc_B, GPIO.IN)
    GPIO.add_event_detect(Enc_A, GPIO.RISING, callback=rotation_decode, bouncetime=2) 
    return

def rotation_decode(Enc_A):

    global temp
    sleep(0.002) 
    
    Switch_A = GPIO.input(Enc_A)
    Switch_B = GPIO.input(Enc_B)

    if (Switch_A == 1) and (Switch_B == 0) :
        if temp <= 99:
            temp += 1
        elif temp >= 99:
            sleep(.001)
        #print "direction -> ", temp
        print temp

        while Switch_B == 0:
            Switch_B = GPIO.input(Enc_B)
      
        while Switch_B == 1:
            Switch_B = GPIO.input(Enc_B)
        return

    elif (Switch_A == 1) and (Switch_B == 1):
        if temp >= 61:
            temp -= 1
        elif temp <= 60:
            sleep(.001)
        #print "direction <- ", temp
        print temp

        while Switch_A == 1:
            Switch_A = GPIO.input(Enc_A)
        return

    else:
        return



def main():

    try:

        init()
        while True :
            # wait for an encoder click
            sleep(1)

    except KeyboardInterrupt:
        GPIO.cleanup()


if __name__ == '__main__':
    main()