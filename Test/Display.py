#By Andrew Robinson and Chris Burgess

import pygame
import RPi.GPIO as GPIO
from time import sleep
 
temp = 80
Enc_A = 3 
Enc_B = 5 

RED = (255,0,0)
ORANGE = (255,165,0)
YELLOW = (255,255,0)
GREEN = (0,128,0)
BLUE = (100,149,237)
BLACK = (0,0,0)

COLOR = YELLOW
pygame.init()
size = (640, 480)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Lyte")
clock = pygame.time.Clock()
pygame.mouse.set_visible(False)

def screenupdate():
    
    if temp <= 69:
        COLOR = BLUE
    elif temp <= 78 and temp > 69:
        COLOR = GREEN
    elif temp <= 87 and temp >78:
        COLOR = YELLOW
    elif temp <= 96 and temp > 87:
        COLOR = ORANGE
    elif temp <= 105 and temp > 96:
        COLOR = RED
    else:
        COLOR = (255,255,255)
    
    
    screen.fill(COLOR)
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    font = pygame.font.SysFont("Calibri", 420, True, False)
    text = font.render(str(temp), True, BLACK)
    textpos = text.get_rect()
    textpos.centerx = background.get_rect().centerx
    textpos.centery = background.get_rect().centery
    screen.blit(text, textpos)
    pygame.display.flip()
    clock.tick(60)

def init():

    print "Lyte Shower Temperature Control Software"
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(Enc_A, GPIO.IN) 
    GPIO.setup(Enc_B, GPIO.IN)
    GPIO.add_event_detect(Enc_A, GPIO.RISING, callback=rotation_decode, bouncetime=2) 
    return

def rotation_decode(Enc_A):

    global temp
    sleep(0.002) 
    
    Switch_A = GPIO.input(Enc_A)
    Switch_B = GPIO.input(Enc_B)

    if (Switch_A == 1) and (Switch_B == 0) :
        if temp <= 104:
            temp += 1
            screenupdate()
        elif temp >= 104:
            sleep(.001)
        #print "direction -> ", temp
        print temp

        while Switch_B == 0:
            Switch_B = GPIO.input(Enc_B)
      
        while Switch_B == 1:
            Switch_B = GPIO.input(Enc_B)
        return

    elif (Switch_A == 1) and (Switch_B == 1):
        if temp >= 61:
            temp -= 1
            screenupdate()
        elif temp <= 60:
            sleep(.001)
        #print "direction <- ", temp
        print temp

        while Switch_A == 1:
            Switch_A = GPIO.input(Enc_A)
        return

    else:
        return
 

 
def main():
    

    try:

        init()
        while True :
            # wait for an encoder click
            sleep(1)

    except KeyboardInterrupt:
        GPIO.cleanup()


if __name__ == '__main__':
    main()
    
pygame.quit()